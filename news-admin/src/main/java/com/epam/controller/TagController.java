package com.epam.controller;


import by.epam.lab.task.entity.Tag;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.ws.INewsWS;
import by.epam.lab.task.ws.ITagWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class TagController {

    @Autowired
    INewsWS newsWS;
    @Autowired
    ITagWS tagWS;
    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public String printNews(ModelMap model) throws ServiceException {
        List<Tag> tagList = tagWS.readAll();
        model.addAttribute("tagList", tagList);
        model.addAttribute("tag", new Tag());
        return "tag_index";
    }


    @RequestMapping(value = "/tag/delete", method = RequestMethod.POST)
    public String deleteTag(@ModelAttribute Tag tag) throws ServiceException {
        newsWS.deleteTag(tag);
        return "redirect:/tags";
    }

    @RequestMapping(value = "/tag/create", method = RequestMethod.POST)
    public String createTag(@ModelAttribute Tag tag) throws ServiceException {
        System.out.println("creating tag:" + tag);
        tagWS.createTag(tag);

        return "redirect:/tags";
    }

    @RequestMapping(value = "/tag/update", method = RequestMethod.POST)
    public String updateTag(@ModelAttribute Tag tag) throws ServiceException {
        tagWS.update(tag);
        return "redirect:/tags";
    }



}
