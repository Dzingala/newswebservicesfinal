package by.epam.lab.task.ws;

import by.epam.lab.task.entity.News;
import by.epam.lab.task.entity.SearchCriteria;
import by.epam.lab.task.entity.Tag;
import by.epam.lab.task.entity.dto.NewsTO;
import by.epam.lab.task.entity.dto.NewsTORecord;
import by.epam.lab.task.exceptions.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

/**
 * @author Ivan_Dzinhala
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface INewsWS {
    /**
     * Get all news existing in database.
     * @return the list of all news.
     * @throws by.epam.lab.task.exceptions.service.ServiceException
     */
    @WebMethod ArrayList<News> readAll() throws ServiceException;

    /**
     * Get all news that are sorted by comments.
     * @return the list of news which are sorted by comments
     * @throws ServiceException
     */
    @WebMethod ArrayList<News> readSortedByComments() throws ServiceException;

    /**
     * Get all news that are sorted by search criteria.
     * @param searchCriteria
     * @return the list of news sorted by the search criteria
     * @throws ServiceException
     */
    @WebMethod ArrayList<News> readBySearchCriteria(SearchCriteria searchCriteria, Long page) throws ServiceException;

    /**
     * Add news and all data that is connected
     * with news (author, tag ...) to database.
     * @param newsTO
     * @throws ServiceException
     */
    @WebMethod void create(NewsTO newsTO)throws ServiceException;

    /**
     * Get news and all data that is connected
     * with news by news id.
     * @param id
     * @throws ServiceException
     * @return NewsTO object consisting of all information required.
     */
    @WebMethod NewsTO readDataByNewsId(Long id)throws ServiceException;

    /**
     * Edit news and data that is connected
     * with that news.
     * @param newsTO
     * @throws ServiceException
     */
    @WebMethod void update(NewsTO newsTO)throws ServiceException;

    /**
     * Delete news and data that is connected
     * with that news.
     * @param newsTO
     * @throws ServiceException
     */
    @WebMethod void delete(NewsTO newsTO)throws ServiceException;

    /**
     * Get all information concerning news by news id in record format.
     * @param newsId
     * @return NewsTORecord object consisting of all information required in record form.
     * @throws ServiceException
     */
    @WebMethod NewsTORecord getNewsForEditing(Long newsId)throws ServiceException;

    /**
     * Update all information concerning certain piece of news given in record form.
     * @param newsTORecord
     * @throws ServiceException
     */
    @WebMethod void updateNews(NewsTORecord newsTORecord)throws ServiceException;

    /**
     * Deletes tag and disconnects it with the piece of news.
     * @param tag
     * @throws ServiceException
     */
    @WebMethod void deleteTag(Tag tag)throws ServiceException;

    /**
     * Counts the amount of pages necessary.
     * @return The amount of pages depending on the amount of news.
     * @throws ServiceException
     */
    @WebMethod Long countPages()throws ServiceException;

    /**
     * Make query for getting the count of pages according certain search criteria.
     * @param searchCriteria
     * @return Query that is ready for injection.
     */
    @WebMethod String composeCriteriaNewsAmountQuery(SearchCriteria searchCriteria);

    /**
     * Count pages suitable for search criteria.
     * @param searchCriteria
     * @return The amount of pages suitable for search criteria.
     * @throws ServiceException
     */
    @WebMethod Long getCriteriaPagesAmount(SearchCriteria searchCriteria,Long page)throws ServiceException;

    /**
     * Creates news with all information concerned according to the certain news record.
     * @param newsTORecord
     * @throws ServiceException
     */
    @WebMethod
    void createNews(NewsTORecord newsTORecord) throws ServiceException;
}
