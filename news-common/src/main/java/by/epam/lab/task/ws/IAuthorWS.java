package by.epam.lab.task.ws;

import by.epam.lab.task.entity.Author;
import by.epam.lab.task.exceptions.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

/**
 * @author Ivan_Dzinhala
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IAuthorWS {
    /**
     * Get author from database by id.
     * @param id
     * @return required author.
     * @throws by.epam.lab.task.exceptions.service.ServiceException
     */
    @WebMethod Author read (Long id) throws ServiceException;
    /**
     * Read all authors from database.
     * @return list of all authors
     * @throws ServiceException
     */
    @WebMethod ArrayList<Author> readAll() throws ServiceException;
    /**
     * Add author to database.
     * @param author
     * @throws ServiceException
     * @return id of row that has been created
     */
    @WebMethod Long create (Author author) throws ServiceException;

    /**
     * Get author by news id.
     * @param newsId
     * @throws ServiceException
     * @return author
     */
    @WebMethod Author readByNewsId(Long newsId) throws ServiceException;

    /**
     * Make the author expired.
     * @param author
     * @throws ServiceException
     */
    @WebMethod void delete(Author author) throws ServiceException;

    /**
     * Update information about certain author.
     * @param author
     * @throws ServiceException
     */
    @WebMethod void update(Author author) throws ServiceException;
}
