package by.epam.lab.task.ws.impl;

import by.epam.lab.task.entity.Role;
import by.epam.lab.task.exceptions.dao.NoSuchEntityException;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.service.RolesService;
import by.epam.lab.task.ws.IRoleWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

/**
 * @author Ivan_Dzinhala
 */
@Component
@WebService
public class RoleWSImpl implements IRoleWS{
    @Autowired
    private RolesService rolesService;

    @Override
    public Long create(Role role) throws ServiceException {
        return rolesService.create(role);
    }

    @Override
    public Role readById(Long roleId) throws ServiceException, NoSuchEntityException {
        return rolesService.readById(roleId);
    }

    @Override
    public void update(Role role) throws ServiceException {
        rolesService.update(role);
    }

    @Override
    public void delete(Role role) throws ServiceException {
        rolesService.delete(role);
    }
}
