package by.epam.lab.task.ws.impl;

import by.epam.lab.task.entity.Author;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.service.AuthorService;
import by.epam.lab.task.ws.IAuthorWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.ArrayList;

/**
 * @author Ivan_Dzinhala
 */
@Component
@WebService
public class AuthorWSImpl implements IAuthorWS{
    @Autowired
    private AuthorService authorService;

    @Override
    public Author read(Long id) throws ServiceException {
        return authorService.read(id);
    }

    @Override
    public ArrayList<Author> readAll() throws ServiceException {
        return authorService.readAll();
    }

    @Override
    public Long create(Author author) throws ServiceException {
        return authorService.create(author);
    }

    @Override
    public Author readByNewsId(Long newsId) throws ServiceException {
        return authorService.readByNewsId(newsId);
    }

    @Override
    public void delete(Author author) throws ServiceException {
        authorService.delete(author);
    }

    @Override
    public void update(Author author) throws ServiceException {
        authorService.update(author);
    }
}
