package by.epam.lab.task.ws;


import by.epam.lab.task.entity.Tag;
import by.epam.lab.task.exceptions.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ITagWS {
    /**
     * returns helloworld string
     * @return Hello, world!
     */
    @WebMethod public String getHelloWorld();
    /**
     * Add tag to database.
     * Validate parameters.
     * @param tag
     * @throws ServiceException
     * @return id of row that has been inserted
     */
    @WebMethod public void createTag(Tag tag) throws ServiceException;
    /**
     * Read all tags from database.
     * @return list of all tags
     * @throws ServiceException
     */
    @WebMethod public List<Tag> readAll() throws ServiceException;
    /**
     * Get tag by its id
     * @param id
     * @throws ServiceException
     * @return tag that was read by id
     */
    @WebMethod Tag readById(Long id)throws ServiceException;

    /**
     * Get all tags by news id.
     * @param newsId
     * @throws ServiceException
     */
    @WebMethod List<Tag> readTagsByNewsId(Long newsId)throws ServiceException;

    /**
     * Update tag's content.
     * @param tag
     * @throws ServiceException
     */
    @WebMethod void update(Tag tag)throws ServiceException;

    /**
     * Delete tag from database.
     * @param tag
     * @throws ServiceException
     */
    @WebMethod void delete(Tag tag)throws ServiceException;

    /**
     * Read all news' id by tag id given.
     * @param tagId
     * @return
     * @throws ServiceException
     */
    @WebMethod List<Long> readNewsIdByTagId(Long tagId)throws ServiceException;
}
