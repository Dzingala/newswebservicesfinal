package by.epam.lab.task.ws.impl;

import by.epam.lab.task.entity.News;
import by.epam.lab.task.entity.SearchCriteria;
import by.epam.lab.task.entity.Tag;
import by.epam.lab.task.entity.dto.NewsTO;
import by.epam.lab.task.entity.dto.NewsTORecord;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.service.NewsService;
import by.epam.lab.task.ws.INewsWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.ArrayList;

/**
 * @author Ivan_Dzinhala
 */
@Component
@WebService
public class NewsWSImpl implements INewsWS{
    @Autowired
    private NewsService newsService;

    @Override
    public ArrayList<News> readAll() throws ServiceException {
        return newsService.readAll();
    }

    @Override
    public ArrayList<News> readSortedByComments() throws ServiceException {
        return newsService.readSortedByComments();
    }

    @Override
    public ArrayList<News> readBySearchCriteria(SearchCriteria searchCriteria, Long page) throws ServiceException {
        return newsService.readBySearchCriteria(searchCriteria,page);
    }

    @Override
    public void create(NewsTO newsTO) throws ServiceException {
        newsService.create(newsTO);
    }

    @Override
    public NewsTO readDataByNewsId(Long id) throws ServiceException {
        return newsService.readDataByNewsId(id);
    }

    @Override
    public void update(NewsTO newsTO) throws ServiceException {
        newsService.update(newsTO);
    }

    @Override
    public void delete(NewsTO newsTO) throws ServiceException {
        newsService.delete(newsTO);
    }

    @Override
    public NewsTORecord getNewsForEditing(Long newsId) throws ServiceException {
        return newsService.getNewsForEditing(newsId);
    }

    @Override
    public void updateNews(NewsTORecord newsTORecord) throws ServiceException {
        newsService.updateNews(newsTORecord);
    }

    @Override
    public void deleteTag(Tag tag) throws ServiceException {
        newsService.deleteTag(tag);
    }

    @Override
    public Long countPages() throws ServiceException {
        return newsService.countPages();
    }

    @Override
    public String composeCriteriaNewsAmountQuery(SearchCriteria searchCriteria) {
        return newsService.composeCriteriaNewsAmountQuery(searchCriteria);
    }

    @Override
    public Long getCriteriaPagesAmount(SearchCriteria searchCriteria, Long page) throws ServiceException {
        return newsService.getCriteriaPagesAmount(searchCriteria,page);
    }

    @Override
    public void createNews(NewsTORecord newsTORecord) throws ServiceException {
        newsService.createNews(newsTORecord);
    }
}
