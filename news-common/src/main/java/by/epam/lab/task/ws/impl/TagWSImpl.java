package by.epam.lab.task.ws.impl;

import by.epam.lab.task.entity.Tag;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.service.TagService;
import by.epam.lab.task.ws.ITagWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Ivan_Dzinhala
 */
@Component
@WebService
public class TagWSImpl implements ITagWS{
    @Autowired
    private TagService tagService;

    @Override
    public String getHelloWorld() {
        return "Hello, world!";
    }

    @Override
    public void createTag(Tag tag) throws ServiceException {
        System.out.println("tag got: "+tag);
        tagService.create(tag);
    }
    @Override
    public List<Tag> readAll()throws ServiceException{
        return tagService.readAll();
    }

    @Override
    public Tag readById(Long id) throws ServiceException {
        return tagService.readById(id);
    }

    @Override
    public List<Tag> readTagsByNewsId(Long newsId) throws ServiceException {
        return tagService.readTagsByNewsId(newsId);
    }

    @Override
    public void update(Tag tag) throws ServiceException {
        tagService.update(tag);
    }

    @Override
    public void delete(Tag tag) throws ServiceException {
        tagService.delete(tag);
    }

    @Override
    public List<Long> readNewsIdByTagId(Long tagId) throws ServiceException {
        return tagService.readNewsIdByTagId(tagId);
    }
}
