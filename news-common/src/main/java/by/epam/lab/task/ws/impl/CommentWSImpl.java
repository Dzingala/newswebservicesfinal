package by.epam.lab.task.ws.impl;

import by.epam.lab.task.entity.Comment;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.service.CommentService;
import by.epam.lab.task.ws.ICommentWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.ArrayList;

/**
 * @author Ivan_Dzinhala
 */
@Component
@WebService
public class CommentWSImpl implements ICommentWS{
    @Autowired
    private CommentService commentService;

    @Override
    public Long create(Comment comment) throws ServiceException {
        return commentService.create(comment);
    }

    @Override
    public ArrayList<Comment> readAllByNewsId(Long newsId) throws ServiceException {
        return commentService.readAllByNewsId(newsId);
    }

    @Override
    public void delete(Comment comment) throws ServiceException {
        commentService.delete(comment);
    }

    @Override
    public void update(Comment comment) throws ServiceException {
        commentService.update(comment);
    }

    @Override
    public Comment read(Long commentId) throws ServiceException {
        return commentService.read(commentId);
    }
}
