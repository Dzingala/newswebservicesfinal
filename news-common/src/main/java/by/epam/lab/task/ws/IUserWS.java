package by.epam.lab.task.ws;

import by.epam.lab.task.entity.dto.UserTO;
import by.epam.lab.task.exceptions.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author Ivan_Dzinhala
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IUserWS {
    /**
     * Registration user in the database.
     * Validate parameters.
     * @param userTO
     * @return UserTO
     * @throws by.epam.lab.task.exceptions.service.ServiceException
     */
    @WebMethod UserTO registration(UserTO userTO) throws ServiceException;

    /**
     * Login in the system.
     * @param login
     * @param password
     * @return UserTO
     * @throws ServiceException
     */
    @WebMethod UserTO login(String login, String password) throws ServiceException;

    /**
     * Delete the user from the database.
     * @param userTO
     * @throws ServiceException
     */
    @WebMethod void delete(UserTO userTO) throws ServiceException;

    /**
     * Edit an information about user.
     * Validate parameters.
     * @param userTO
     * @throws ServiceException
     */
    @WebMethod
    void edit(UserTO userTO) throws ServiceException;
}
