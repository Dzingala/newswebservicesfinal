package by.epam.lab.task.ws;

import by.epam.lab.task.entity.Role;
import by.epam.lab.task.exceptions.dao.NoSuchEntityException;
import by.epam.lab.task.exceptions.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author Ivan_Dzinhala
*/

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IRoleWS {
    /**
     * Add role to database.
     * @param role
     * @throws by.epam.lab.task.exceptions.service.ServiceException
     * @return id of row that has been added
     */
    @WebMethod Long create(Role role) throws ServiceException;

    /**
     * Get role by it's id, if it exists.
     * @param roleId
     * @throws ServiceException
     * @return required role
     */
    @WebMethod Role readById(Long roleId) throws ServiceException, NoSuchEntityException;

    /**
     * Edit role.
     * @param role
     * @throws ServiceException
     */
    @WebMethod void update(Role role) throws ServiceException;

    /**
     * Delete role.
     * @param role
     * @throws ServiceException
     */
    @WebMethod void delete(Role role) throws ServiceException;
}
