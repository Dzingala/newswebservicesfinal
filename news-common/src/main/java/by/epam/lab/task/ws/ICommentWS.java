package by.epam.lab.task.ws;

import by.epam.lab.task.entity.Comment;
import by.epam.lab.task.exceptions.service.ServiceException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

/**
 * @author Ivan_Dzinhala
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ICommentWS {
    /**
     * Add comment to database.
     * @param comment
     * @throws by.epam.lab.task.exceptions.service.ServiceException
     * @return comment id
     */
    @WebMethod Long create (Comment comment) throws ServiceException;

    /**
     * Get all comments for that news.
     * @param newsId
     * @throws ServiceException
     * @return list of comments
     */
    @WebMethod ArrayList<Comment> readAllByNewsId(Long newsId) throws ServiceException;

    /**
     * Delete comment from database.
     * @param comment
     * @throws ServiceException
     */
    @WebMethod void delete(Comment comment) throws ServiceException;

    /**
     * Update comment content.
     * @param comment
     * @throws ServiceException
     */
    @WebMethod void update(Comment comment) throws ServiceException;

    /**
     * Get comment by id.
     * @param commentId
     * @throws ServiceException
     */
    @WebMethod Comment read(Long commentId)throws ServiceException;
}
