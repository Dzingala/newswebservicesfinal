package by.epam.lab.task.ws.impl;

import by.epam.lab.task.entity.dto.UserTO;
import by.epam.lab.task.exceptions.service.ServiceException;
import by.epam.lab.task.service.UserService;
import by.epam.lab.task.ws.IUserWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

/**
 * @author Ivan_Dzinhala
 */
@Component
@WebService
public class UserWSImpl implements IUserWS{
    @Autowired
    private UserService userService;

    @Override
    public UserTO registration(UserTO userTO) throws ServiceException {
        return userService.registration(userTO);
    }

    @Override
    public UserTO login(String login, String password) throws ServiceException {
        return userService.login(login,password);
    }

    @Override
    public void delete(UserTO userTO) throws ServiceException {
        userService.delete(userTO);
    }

    @Override
    public void edit(UserTO userTO) throws ServiceException {
        userService.edit(userTO);
    }
}
